<?php
/*
Plugin Name: FAB Serviços - mu-plugin
Plugin URI: https://fabservicos.com.br
Description: Customizations for fabservicos.com.br site (WordPress CPT and CPF)
Author: Cimbre
Version: 1.0.0.1
Author URI: https://cimbre.com.br
Text Domain: fabservicoscombr
*/

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded', function () {
        load_muplugin_textdomain('fabservicoscombr', basename(dirname(__FILE__)) . '/languages');
    }
);

/**
 * Hide editor from all pages
 */
add_action(
    'admin_init', function () {
        remove_post_type_support('page', 'editor');
    }
);

/*********************************************************************************** 
 * Callback Functions 
 **********************************************************************************/

 /**
 * Metabox for Page Slug
 * @author Tom Morton
 * @link https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
 *
 * @param bool $display
 * @param array $meta_box
 * @return bool display metabox
 */
function be_metabox_show_on_slug($display, $meta_box) 
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']);
}
add_filter('cmb2_show_on', 'be_metabox_show_on_slug', 10, 2);

/**
 * Gets a number of terms and displays them as options
* 
* @param CMB2_Field $field 
* 
* @return array An array of options that matches the CMB2 options array
*/
function fabservicos_getTermOptions($field) 
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms) ) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }
    return $term_options;
}

/*********************************************************************************** 
 * Pages Custom Fields
 * ********************************************************************************/

/**
 * Front-page 
 */
add_action(
    'cmb2_admin_init', function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_fabservicoscombr_frontpage_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'fabservicoscombr_frontpage_hero_id',
                'title'         => __('Hero', 'fabservicoscombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
        
        //Hero Group
        $hero_id = $cmb_hero->add_field( 
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '', 
                // 'repeatable'  => false, // use false if you want non-repeatable group
                'options'     => array(
                    'group_title'   => __('Slide {#}', 'fabservicoscombr'),
                    'add_button'    => __('Add Another Slide', 'fabservicoscombr'),
                    'remove_button' => __('Remove Slide', 'fabservicoscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );
        
        //Hero Image
        $cmb_hero->add_group_field( 
            $hero_id, array(
                'name'        => __('Background Image', 'fabservicoscombr'),
                'description' => '',
                'id'          => 'bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                        'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                        'add_upload_file_text' => __('Add Image', 'fabservicoscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                        //'type' => 'image/jpeg', // Make library only display PDFs.
                        // Or only allow gif, jpg, or png images
                        'type' => array(
                          //'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Title
        $cmb_hero->add_group_field( 
            $hero_id, array(
                'name'       => __('Title', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );

        //Hero Text
        $cmb_hero->add_group_field( 
            $hero_id, array(
                'name'       => __('Text', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => 'text',
                'type'       => 'textarea_code',
            )
        );

        //Hero Button Text
        $cmb_hero->add_group_field( 
            $hero_id, array(
                'name'       => __('Button Text', 'fabservicoscombr'),
                'desc'       => '',
                'default'    => __('Know more', 'fabservicoscombr'),
                'id'         => 'btn_text',
                'type'       => 'text_small',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field( 
            $hero_id, array(
                'name'       => __('Button URL', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );

        /**
         * About
         */
        $cmb_about = new_cmb2_box(
            array(
                'id'            => 'fabservicoscombr_frontpage_about_id',
                'title'         => __('About', 'fabservicoscombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //About Title
        $cmb_about->add_field( 
            array(
                'name'       => __('About Title', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_title',
                'type'       => 'text',
            )
        );

        //About Subtitle
        $cmb_about->add_field( 
            array(
                'name'       => __('About Subtitle', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_subtitle',
                'type'       => 'text',
            )
        );

        //About Content Image
        $cmb_about->add_field(
            array (
                'name'        => __('About Content Image', 'fabservicoscombr'),
                'description' => '',
                'id'          => $prefix . 'about_content_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                        'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                        'add_upload_file_text' => __('Add Image', 'fabservicoscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                        //'type' => 'image/jpeg', // Make library only display PDFs.
                        // Or only allow gif, jpg, or png images
                        'type' => array(
                          //'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //About Content Title
        $cmb_about->add_field( 
            array(
                'name'       => __('About Content Title', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_content_title',
                'type'       => 'text',
            )
        );

        //About Content Text
        $cmb_about->add_field( 
            array(
                'name'       => __('About Content Text', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_content_text',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Services
         ******/
        $cmb_services = new_cmb2_box(
            array(
                'id'            => 'fabservicoscombr_frontpage_services_id',
                'title'         => __('Services', 'fabservicoscombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Services Title
        $cmb_services->add_field( 
            array(
                'name'       => __('Services Title', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'services_title',
                'type'       => 'text',
            )
        );

        //Services Subtitle
        $cmb_services->add_field( 
            array(
                'name'       => __('Services Subtitle', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'services_subtitle',
                'type'       => 'text',
            )
        );

        /******
         * Parallax
         ******/
        $cmb_parallax = new_cmb2_box(
            array(
                'id'            => 'fabservicoscombr_frontpage_parallax_id',
                'title'         => __('Parallax', 'fabservicoscombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Quote
        $cmb_parallax->add_field( 
            array(
                'name'       => __('Quote', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'parallax_quote',
                'type'       => 'textarea_code',
            )
        );

        //Author
        $cmb_parallax->add_field( 
            array(
                'name'       => __('Author', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'parallax_author',
                'type'       => 'text',
            )
        );

        /**
         * News
         */
        $cmb_news = new_cmb2_box(
            array(
                'id'            => 'fabservicoscombr_frontpage_news_id',
                'title'         => __('News', 'fabservicoscombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //News Title
        $cmb_news->add_field( 
            array(
                'name'       => __('News Title', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'news_title',
                'type'       => 'text',
            )
        );

        //News Subtitle
        $cmb_news->add_field( 
            array(
                'name'       => __('News Subtitle', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'news_subtitle',
                'type'       => 'text',
            )
        );

        //Number of News 
        $cmb_news->add_field( 
            array(
                'name'       => __('Number of news', 'fabservicoscombr'),
                'desc'       => __('Number of news to show', 'fabservicoscombr'),
                'id'         => $prefix . 'news_number',
                'type'       => 'text',
                'attributes' => array(
                    'type' => 'number',
                    'min' => '3',
                    'max' => '9',
                    'step' => '3',
                ),
            )
        );

        //Hero Button Text
        $cmb_news->add_field( 
            array(
                'name'       => __('Button Text', 'fabservicoscombr'),
                'desc'       => '',
                'default'    => __('Know more', 'fabservicoscombr'),
                'id'         => $prefix . 'news_btn_text',
                'type'       => 'text_small',
            )
        );

        /**
         * Contact
         */
        $cmb_contact = new_cmb2_box(
            array(
                'id'            => 'fabservicoscombr_frontpage_contact_id',
                'title'         => __('Contact', 'fabservicoscombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Contact Title
        $cmb_contact->add_field( 
            array(
                'name'       => __('Contact Title', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_title',
                'type'       => 'text',
            )
        );

        //Contact Subtitle
        $cmb_contact->add_field( 
            array(
                'name'       => __('Contact Subtitle', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_subtitle',
                'type'       => 'text',
            )
        );

        //Contact Form Shortcode
        $cmb_contact->add_field( 
            array(
                'name'       => __('Contact Form Shortcode', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_form_shortcode',
                'type'       => 'text',
            )
        );

        //Contact Map URL
        $cmb_contact->add_field( 
            array(
                'name'       => __('Google Maps URL', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_map_url',
                'type'       => 'textarea_code',
            )
        );
    }
);

/**
 * Services Custom Fields
 */
add_action(
    'cmb2_admin_init', function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_fabservicoscombr_service_';
        
        /**
        * Home
        */
        $cmb_home = new_cmb2_box(
            array(
                'id'            => 'fabservicoscombr_service_home_id',
                'title'         => __('Home', 'fabservicoscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       =>  array('key' => 'page-template', 'value' => 'views/template-servicos.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
        
        //Home Image
        $cmb_home->add_field( 
            array(
                'name'        => __('Image in Home', 'fabservicoscombr'),
                'description' => '',
                'id'          => $prefix . 'home_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                        'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                        'add_upload_file_text' => __('Add Image', 'fabservicoscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                        //'type' => 'image/jpeg', // Make library only display PDFs.
                        // Or only allow gif, jpg, or png images
                        'type' => array(
                          //'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Home Title
        $cmb_home->add_field( 
            array(
                'name'       => __('Title in Home', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'home_title',
                'type'       => 'text',
            )
        );

        //Home Text
        $cmb_home->add_field( 
            array(
                'name'       => __('Text in Home', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'home_text',
                'type'       => 'textarea_code',
            )
        );

        //Home Button Text
        $cmb_home->add_field( 
            array(
                'name'       => __('Button Text', 'fabservicoscombr'),
                'desc'       => '',
                'default'    => __('Know more', 'fabservicoscombr'),
                'id'         => $prefix . 'home_btn_text',
                'type'       => 'text_small',
            )
        );

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'fabservicoscombr_service_hero_id',
                'title'         => __('Hero', 'fabservicoscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       =>  array('key' => 'page-template', 'value' => 'views/template-servicos.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
        
        //Hero Title
        $cmb_hero->add_field( 
            array(
                'name'       => __('Hero Title', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_title',
                'type'       => 'text',
            )
        );

        //Hero Text
        $cmb_hero->add_field( 
            array(
                'name'       => __('Hero Text', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_text',
                'type'       => 'textarea_code',
            )
        );

        /**
        * Content
        */
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'fabservicoscombr_service_content_id',
                'title'         => __('Content', 'fabservicoscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       =>  array('key' => 'page-template', 'value' => 'views/template-servicos.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Section Title
        $cmb_content->add_field( 
            array(
                'name'       => __('Title', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'text',
            )
        );

        //Section Subtitle
        $cmb_content->add_field( 
            array(
                'name'       => __('Subtitle', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'text',
            )
        );

        //Featured Image
        $cmb_content->add_field( 
            array(
                'name'        => __('Image', 'fabservicoscombr'),
                'description' => '',
                'id'          => $prefix . 'content_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                        'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                        'add_upload_file_text' => __('Add Image', 'fabservicoscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                        //'type' => 'image/jpeg', // Make library only display PDFs.
                        // Or only allow gif, jpg, or png images
                        'type' => array(
                          //'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Text
        $cmb_content->add_field( 
            array(
                'name'       => __('Text', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_text',
                'type'       => 'textarea_code',
            )
        );

        /**
        * Services Provided
        */
        $cmb_provided = new_cmb2_box(
            array(
                'id'            => 'fabservicoscombr_service_provided_id',
                'title'         => __('Services Provided', 'fabservicoscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       =>  array('key' => 'page-template', 'value' => 'views/template-servicos.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Title
        $cmb_provided->add_field( 
            array(
                'name'       => __('Title', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'provided_title',
                'type'       => 'text',
            )
        );

        //Services Group
        $provided_id = $cmb_provided->add_field( 
            array(
                'id'          => $prefix . 'provided_services',
                'type'        => 'group',
                'description' => '', 
                // 'repeatable'  => false, // use false if you want non-repeatable group
                'options'     => array(
                    'group_title'   => __('Service {#}', 'fabservicoscombr'),
                    'add_button'    => __('Add Another Service', 'fabservicoscombr'),
                    'remove_button' => __('Remove Service', 'fabservicoscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );
        
        //Service Name
        $cmb_provided->add_group_field( 
            $provided_id, array(
                'name' => __('Name', 'fabservicoscombr'),
                'id'   => 'service_name',
                'type' => 'text',
            ) 
        );

        //Service Description
        $cmb_provided->add_group_field( 
            $provided_id, array(
            'name' => __('Description', 'fabservicoscombr'),
            'id'   => 'service_desc',
            'description' => '',
            'type' => 'textarea_code',
            ) 
        );

        //Service Anchor URL
        $cmb_provided->add_group_field( 
            $provided_id, array(
            'name' => __('Destination anchor', 'fabservicoscombr'),
            'id'   => 'service_anchor',
            'description' => '',
            'type' => 'text_small',
            ) 
        );
    }
);

/**
 * Home (Blog)
 */
add_action(
    'cmb2_admin_init', function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_fabservicoscombr_blog_';

        /******
         * Hero
         ******/
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'home_hero_id',
                'title'         => __('Hero', 'fabservicoscombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'blog'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Hero Title
        $cmb_hero->add_field( 
            array(
                'name'       => __('Hero Title', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_title',
                'type'       => 'text',
                'default'    => 'Blog',
            )
        );

        //Hero Text
        $cmb_hero->add_field( 
            array(
                'name'       => __('Hero Text', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_text',
                'type'       => 'text',
                'default'    => 'Blog',
            )
        );
        
    }
);

/**
 * Single Post (Blog)
 */
add_action(
    'cmb2_admin_init', function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_fabservicoscombr_post_';

        /******
         * Hero
         ******/
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'post_hero_id',
                'title'         => __('Hero', 'fabservicoscombr'),
                'object_types'  => array('post'), // post type
                //'show_on'       =>  array('key' => 'page-template', 'value' => 'views/home.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Hero Title
        $cmb_hero->add_field( 
            array(
                'name'       => __('Hero Title', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_title',
                'type'       => 'text',
                'default'    => 'Blog',
            )
        );

        /**
         * Content
         */
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'post_content_id',
                'title'         => __('Content', 'fabservicoscombr'),
                'object_types'  => array('post'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'clinica'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Content Subtitle
        $cmb_content->add_field( 
            array(
                'name'       => __('Content Subtitle', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'text',
            )
        );

        //Content Subtitle
        $cmb_content->add_field( 
            array(
                'name'       => __('Summary of content', 'fabservicoscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_summary',
                'type'       => 'textarea_code',
            )
        );
    }
);
?>
